====================
Driver script for matrix of gas sensors
====================


Driver operates on the matrix of gas sensors and gas dispensers. It's purpose is to obtain and save
electrical resistance of a sensor and it's temperature. 

Description
===========

This project is a part of a Bachelor Thesis "Development and implementation of the measuring system for gas sensors".
This is a project of system for measurement of gas sensors’ parameters and communication with gas dispensers via RS232. The station provides possibility to conduct parallel resistance and temperature measurement for four gas sensors and has four DB9 sockets for serial communication protocol. The project allows for measuring resistances from 100Ω to 500MΩ range and temperatures from 20°C to 350°C. 

