import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly
from scipy import stats
import csv


class LoadPlot:
    def __init__(self, filename):
        self.filename = filename
        self.open_and_plot(filename)

    @staticmethod
    def open_and_plot(file_name):
        channel = file_name[-5]
        voltage = []
        seconds = []
        temperature = []
        resistance = []
        av_temp = []
        av_res = []
        av_time = []

        try:
            with open(file_name, 'r') as csvfile:
                data = csv.reader(csvfile, delimiter=",")
                if file_name[-7] == 't':
                    for row in data:
                        if row:
                            seconds.append(row[0])
                            voltage.append(row[1])
                    for sample in voltage:
                        data = sample.split(',')
                        if float(data[0]) != 0.0:
                            temperature.append(LoadPlot.pt100_temp_calculation(float(data[0])))
                        else:
                            temperature.append(LoadPlot.pt100_temp_calculation(1.0))
                    seconds = np.array(seconds,dtype=float)

                    try:
                        av_temp.append(np.nanmean(np.pad((np.array(temperature)).astype(float),
                                                         (0, 3 - (np.array(temperature)).size%3),
                                                         mode='constant',
                                                         constant_values=np.NaN).reshape(-1, 3),
                                                  axis=1))
                        av_time.append(np.nanmean(np.pad((np.array(seconds)).astype(float),
                                                         (0, 3 - (np.array(seconds)).size%3),
                                                         mode='constant',
                                                         constant_values=np.NaN).reshape(-1, 3),
                                                  axis=1))
                    except IndexError:
                        pass

                    av_temp = np.array(av_temp[0])
                    av_time = np.array(av_time[0])

                    slope, intercept, r_value, p_value, std_err = stats.linregress(av_time, av_temp)
                    line = slope*av_time+intercept

                    regression_diff = np.subtract(av_temp, line)

                    standard_deviation = np.std(av_temp)
                    print("Standard deviation: " + str(standard_deviation))

                    plt.plot(av_time, line, 'b--', label='y={:.2f}x+{:.2f}'.format(slope, intercept))
                    plt.errorbar(av_time, av_temp, yerr=regression_diff, fmt=' ', ecolor='gray', uplims=True, alpha=0.5,
                                 label='błąd i aproksymacja do reg. liniowej')
                    plt.scatter(av_time, av_temp, c='r', marker='.',
                                label='odchylenie std.: {:.2f}'.format(standard_deviation))
                    plt.xlim([0.0,seconds[-1]+0.2])
                    plt.title('Dane z pliku ' + file_name + ': \nTemperatura z kanału ' + channel)
                    plt.xlabel('Czas [s]')
                    plt.ylabel('Temperatura [st. C]')
                    plt.legend()
                    plt.show()

                if file_name[-7] == 'r':
                    for row in data:
                        if row:
                            seconds.append(row[0])
                            voltage.append(row[1])
                    for sample in voltage:
                        data = sample.split(',')
                        ref_code = int(data[1])
                        if float(data[0]) != 0.0:
                            resistance.append(LoadPlot.res_calculation(float(data[0]), LoadPlot.select_ref_resistor(ref_code)))
                        else:
                            resistance.append(LoadPlot.res_calculation(1.0, LoadPlot.select_ref_resistor(ref_code)))
                    seconds = np.array(seconds, dtype=float)

                    try:
                        av_res.append(np.nanmean(np.pad((np.array(resistance)).astype(float),
                                                        (0, 3 - (np.array(resistance)).size%3),
                                                        mode='constant',
                                                        constant_values=np.NaN).reshape(-1, 3),
                                                 axis=1))
                        av_time.append(np.nanmean(np.pad((np.array(seconds)).astype(float),
                                                         (0, 3 - (np.array(seconds)).size%3),
                                                         mode='constant',
                                                         constant_values=np.NaN).reshape(-1, 3),
                                                  axis=1))
                    except IndexError:
                        pass

                    av_res = np.array(av_res[0])
                    av_time = np.array(av_time[0])

                    slope, intercept, r_value, p_value, std_err = stats.linregress(av_time, av_res)
                    line = slope*av_time+intercept

                    regression_diff = np.subtract(av_res, line)

                    standard_deviation = np.std(av_res)
                    print("Standard deviation: " + str(standard_deviation))

                    plt.plot(av_time, line, 'b--',
                             label='y={:.2f}x+{:.2f}'.format(slope, intercept))
                    plt.errorbar(av_time, av_res, yerr=regression_diff, fmt=' ', ecolor='gray', uplims=True, alpha=0.5,
                                 label='błąd i aproksymacja do reg. liniowej')
                    plt.scatter(av_time, av_res, c='r', marker='.',
                                label='odchylenie std.: {:.2f}'.format(standard_deviation))
                    plt.xlim([0.0,seconds[-1]+0.2])
                    plt.title('Dane z pliku ' + file_name + ': \nRezystancja czujnika gazu, kanał ' + channel)
                    plt.xlabel('Czas [s]')
                    plt.ylabel('Rezystancja [Ohm]')
                    plt.legend()
                    plt.show()
        except FileNotFoundError:
            print("Cannot find this file. Check the name again.\n")


    @staticmethod
    def pt100_temp_calculation(samp):
        x = (samp/65535)*3.3
        res = ((0.5*x/3.3 + 100/1100)*1000)/(1 - (0.5*x/3.3) -(100/1100))
        temp = (res/100 - 1)/0.008351
        return temp

    @staticmethod
    def res_calculation(samp, ref):
        a = (samp/65535)*3.3
        b = (3.3*ref)/a
        res = b - ref
        return res

    @staticmethod
    def select_ref_resistor(ref_code):
        value = 1
        if ref_code == 0: value = 950
        if ref_code == 1: value = 10000
        if ref_code == 2: value = 100000
        if ref_code == 3: value = 1000000
        if ref_code == 4: value = 10000000
        if ref_code == 5: value = 100000000
        return value
