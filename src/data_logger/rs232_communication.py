__all__ = ["RS232Communication"]


class RS232Communication:
    """
    Class used for serial communication over UART-RS232 converter with gas dispensers.
    """
    def __init__(self, port):
        self.port = port
        pass

    @staticmethod
    def send_command(port):
        command = input("Write command: ")
        try:
            port.write(command.encode())
            response = port.readline()
            print(response)
        except Exception:
            print("Could not send command.\n")
