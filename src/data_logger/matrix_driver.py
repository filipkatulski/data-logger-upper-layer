import serial
import csv
import time

from src.data_logger.find_ports import FindPorts
from src.data_logger.load_plot import LoadPlot
from src.data_logger.rs232_communication import RS232Communication


def communication(ports):

    print("List of ports:")
    for i in ports:
        print(str(ports.index(i)) + " " + i)
    ser = None
    chosen_port = input("Type (or copy) which port "
                        "you want to use ('none' for loading previous measurements):")

    if chosen_port != 'none':
        try:
            ser = serial.Serial(
                port=chosen_port,
                baudrate=9600,
                bytesize=serial.EIGHTBITS,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                timeout=1,
                xonxoff=False,
                rtscts=False,
                dsrdtr=False,
                writeTimeout=2
            )
        except:
            print("Could not open port {portnum} ".format(portnum=chosen_port))
            exit(-1)
    else:
        while True:
            request_code = input("What do you want to do?\n"
                             "'l' - load plot from existing file,\n"
                             "'exit' - terminate the program.\n")
            if request_code == 'l':
                selection = input("Write name of file: ")
                LoadPlot.open_and_plot(selection)
            if request_code == 'exit':
                exit(0)

    while True:
        file_name = 0
        request_code = input("What do you want to do?\n'r' - measure resistance,\n"
                             "'t' - measure temperature,\n"
                             "'s' - RS232 communication,\n"
                             "'l' - load plot from existing file,\n"
                             "'exit' - terminate the program.\n")
        if request_code == 't' or request_code == 'r':
            channel = input("Which channel? 1, 2, 3, 4?\n")
            gather_time = input("Write amount of measurement time [s]: ")
            file_name = input("Write file name: ")
            file_name = file_name + "_" + request_code + "_" + channel + ".csv"
            end_time = time.time() + float(gather_time)
            start_time = time.time()
            while time.time() < end_time:
                try:
                    ser.write(request_code.encode())
                    time.sleep(0.1)
                    ser.write(channel.encode())
                    time.sleep(0.1)
                    ser_bytes = ser.readline()
                    decoded_bytes = ser_bytes[0:len(ser_bytes) - 2].decode("utf-8")
                    print(decoded_bytes)
                    with open(file_name, "a") as f:
                        writer = csv.writer(f, delimiter=",")
                        writer.writerow([round(time.time()-start_time, 2), decoded_bytes])

                except KeyboardInterrupt:
                    print("Keyboard Interrupt")
        if request_code == 'l':
            selection = input("Write name of file: ")
            LoadPlot.open_and_plot(selection)

        if request_code == 's':
            channel = input("Which channel? 1, 2, 3, 4?\n")
            ser.write(request_code.encode())
            time.sleep(0.1)
            ser.write(channel.encode())
            RS232Communication.send_command(port=ser)

        if request_code == "exit":
            exit(0)


if __name__ == '__main__':
    found_ports = FindPorts.serial_ports()
    communication(found_ports)
